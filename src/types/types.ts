type coordinates = {
    x: integer;
    y: integer;
};
type health = {
    cur: integer;
    max: integer;
};
