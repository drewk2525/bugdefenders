import GameScene from "../scenes/GameScene";

export default class Coin {
    scene: GameScene;
    container: Phaser.GameObjects.Container;
    sprite: Phaser.GameObjects.Sprite;
    value: integer;
    hovered: boolean;
    clicked: boolean;
    canCollect: boolean;
    collected: integer;
    constructor(scene, x, y) {
        const container = scene.add.container(x, y);
        container.setSize(32, 32).setInteractive({ useHandCursor: true });
        const coin = scene.add.sprite(0, 0, "coin").setScale(0.5);
        container.add(coin);
        container.setData({
            value: 1,
        });
        this.scene = scene;
        this.container = container;
        this.sprite = coin;
        this.value = 1;
        this.hovered = false;
        this.clicked = false;
        this.canCollect = false;
        this.collected = 0;
        this.createMouseFunctions();
        coin.play({ key: "coin-spin", repeat: -1 });
    }

    createMouseFunctions() {
        this.container.on("pointerup", () => {
            this.scene.gold += 1;
            this.container.destroy();
        });

        this.container.on("pointerdown", () => {
            this.clicked = true;
            this.hovered = true;
        });

        this.container.on("pointerover", () => {
            this.hovered = true;
            if (this.canCollect) {
                this.canCollect = false;
                this.scene.gold += 1;
                this.collected++;
                if (this.collected >= 5) {
                    this.container.destroy();
                    //bug found!
                    //after finding bug, maybe should change entirely to a onhover collection?
                }
            }
        });

        this.container.on("pointerout", () => {
            this.hovered = false;
            this.canCollect = true;
        });
    }
}
