import Coin from "./coin";
import { v4 } from "uuid";
import GameScene from "../scenes/GameScene";

const uuidv4 = v4;

export default class Enemy {
    id: string;
    scene: GameScene;
    container: Phaser.GameObjects.Container;
    destinationNum: integer;
    speed: integer;
    sprite: Phaser.GameObjects.Sprite;
    lastStepDistance: number;
    killValue: integer;
    health: health;
    damage: integer;
    state: string;

    destination: coordinates;
    constructor(scene) {
        const container = scene.add.container(0, 0);
        container.setSize(50, 50).setInteractive();
        const enemy = scene.add.rectangle(0, 0, 50, 50, 0x6666ff);
        scene.physics.world.enable(container);
        container.add(enemy);
        container.setData({
            //never touch the data object directly, instead pass a list of objects to the function.
            destinationNum: 0,
            destination: { x: 0, y: 0 },
            speed: 100,
        });
        this.id = `${uuidv4()}`;
        this.scene = scene;
        this.container = container;
        this.destinationNum = 1;
        this.destination = { x: 0, y: 0 };
        this.speed = 1000;
        this.sprite = enemy;
        this.lastStepDistance = 0;
        this.killValue = 1;
        this.health = { cur: 10, max: 10 };
        this.damage = 1;
        this.state = "moving";
        this.container.on("pointerup", () => {
            this.health.cur -= 5;
        });
    }

    move() {
        const scene = this.scene;
        this.destination = scene.pathSegments[this.destinationNum];
        scene.physics.moveToObject(
            this.container,
            this.destination,
            this.speed
        );
    }

    attackBoat() {
        // Damage boat
        // animation?
        this.death();
    }

    death() {
        this.scene.allEnemies.forEach((enemy, i) => {
            if (enemy.id === this.id) {
                this.scene.allEnemies.splice(i, 1);
            }
        });
        this.container.destroy();
        this.state = "dead";
    }

    update() {
        if (this.health.cur <= 0 && this.state !== "dead") {
            new Coin(this.scene, this.container.x, this.container.y);
            this.death();
            return;
        }

        if (this.state === "dead") {
            return;
        }

        const scene = this.scene;
        const dest = scene.pathSegments[this.destinationNum];

        const pathStart =
            this.destinationNum - 1 > 0
                ? scene.pathSegments[this.destinationNum - 1]
                : { x: 0, y: 0 };
        const pathLength = Phaser.Math.Distance.Between(
            pathStart.x,
            pathStart.y,
            dest.x,
            dest.y
        );
        const distanceTraveled = Phaser.Math.Distance.Between(
            pathStart.x,
            pathStart.y,
            this.container.x,
            this.container.y
        );

        if (distanceTraveled >= pathLength - 10) {
            this.destinationNum++;
            if (this.destinationNum === scene.pathSegments.length) {
                this.attackBoat();
            } else {
                this.move();
            }
        }
    }
}
