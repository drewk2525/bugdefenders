import Phaser from "phaser";

import TitleScene from "./scenes/TitleScene";
import GameScene from "./scenes/GameScene";
import HelloWorldScene from "./scenes/HelloWorldScene";

const width = 1920 / window.devicePixelRatio;
const height = 1080 / window.devicePixelRatio;

const config = {
    type: Phaser.AUTO,
    width,
    height,
    physics: {
        default: "arcade",
        arcade: {
            debug: false,
        },
    },
    scene: [GameScene, TitleScene, HelloWorldScene],
    scale: {
        parent: "gameBody",
        mode: Phaser.Scale.ScaleModes.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 1 / window.devicePixelRatio,
        width: width * window.devicePixelRatio,
        height: height * window.devicePixelRatio,
    },
};

export default new Phaser.Game(config);
