export default class GameMap {
    constructor(scene) {
        const pathSegments = [];
        pathSegments.push({ x: 0, y: 0 });
        pathSegments.push({ x: 100, y: 100 });
        pathSegments.push({ x: 100, y: 500 });
        pathSegments.push({ x: 200, y: 900 });
        pathSegments.push({ x: 600, y: 950 });
        pathSegments.push({ x: 1100, y: 800 });
        pathSegments.push({ x: 1700, y: 800 });
        pathSegments.push({ x: 1750, y: 350 });
        pathSegments.push({ x: 1500, y: 150 });
        pathSegments.push({ x: 1100, y: 100 });
        pathSegments.push({ x: 700, y: 150 });
        pathSegments.push({ x: 500, y: 300 });
        pathSegments.push({ x: 650, y: 600 });

        const paths = [];

        pathSegments.forEach((segment, i) => {
            const nextSegment = pathSegments[i + 1] || scene.boatLocation;
            var graphics = scene.add.graphics();
            const color = 0xa67c52;
            // const rng = new Phaser.Math.RandomDataGenerator();
            // const color = rng.between(0x000000, 0xffffff);
            graphics.lineStyle(50, color, 1);
            const line = graphics.lineBetween(
                segment.x,
                segment.y,
                nextSegment.x,
                nextSegment.y
            );
            paths.push(line);
        });

        pathSegments.push(scene.boatLocation);

        scene.pathSegments = pathSegments;
        scene.paths = paths;
    }
}
