// @ts-nocheck
import Phaser from "phaser";
import Enemy from "../classes/enemy";
import GameMap from "../utils/map";

export default class GameScene extends Phaser.Scene {
    gold: integer;
    spawning: boolean;
    canSpawn: boolean;
    allEnemies: Array;
    allTowers: Array;
    allBugs: Array;
    allObjects: Array;
    pathSegments: Array<coordinates>;
    constructor() {
        super("gameScene");
    }

    preload() {
        this.load.image("gameBackground", "gameBackground.png");
        this.load.spritesheet("coin", "coin.png", {
            frameWidth: 64,
            frameHeight: 64,
        });
    }

    create() {
        this.gold = 0;
        this.spawning = false;
        this.canSpawn = false;
        const background = this.add.image(0, 0, "gameBackground");
        background.setOrigin(0);
        this.boatLocation = { x: 990, y: 500 };

        this.map = new GameMap(this);

        this.allEnemies = [];
        this.allTowers = [];
        this.allBugs = [];
        this.allObjects = [];
        setTimeout(() => {
            this.spawning = true;
            this.canSpawn = true;
        }, 100); // TODO - fix this value - 100 for debugging

        const coinAnimation = this.anims.create({
            key: "coin-spin",
            frames: this.anims.generateFrameNumbers("coin"),
            frameRate: 6,
        });
    }

    update() {
        this.allObjects = [
            ...this.allEnemies,
            ...this.allTowers,
            ...this.allBugs,
        ];
        this.allObjects.forEach((obj) => {
            obj.update();
        });

        this.attemptSpawnEnemy();
    }

    attemptSpawnEnemy() {
        if (!this.spawning || !this.canSpawn) {
            return;
        }
        const enemy = new Enemy(this);
        this.allEnemies.push(enemy);
        enemy.move();
        this.canSpawn = false;
        setTimeout(() => {
            this.canSpawn = true;
        }, 3000);
    }
}
