// @ts-nocheck
import Phaser from "phaser";

export default class TitleScene extends Phaser.Scene {
    constructor() {
        super("title");
    }

    preload() {
        this.load.image("background-title", "Title_Background.png");
    }

    create() {
        const background = this.add.image(0, 0, "background-title");
        background.setOrigin(0);
        const textProps = {
            fontFamily: "sans-serif",
            align: "center",
            fontSize: 80,
            color: "#ffffff",
        };
        const titleText = this.add.text(
            this.scale.width / 2,
            200,
            "Hello World",
            textProps
        );
        titleText.setOrigin(0.5);

        const easyText = this.add.text(this.scale.width / 2, 500, "Easy", {
            ...textProps,
            stroke: "#333333",
            strokeThickness: 3,
        });
        easyText.setOrigin(0.5);
        easyText.setInteractive();

        const medText = this.add.text(this.scale.width / 2, 600, "Medium", {
            ...textProps,
            stroke: "#333333",
            strokeThickness: 3,
        });
        medText.setOrigin(0.5);
        medText.setInteractive();

        const hardText = this.add.text(this.scale.width / 2, 700, "Hard", {
            ...textProps,
            stroke: "#333333",
            strokeThickness: 3,
        });
        hardText.setOrigin(0.5);
        hardText.setInteractive();

        const duration = 80;
        easyText.on("pointerover", () => {
            this.add.tween({
                targets: easyText,
                scaleX: 1.2,
                scaleY: 1.2,
                duration,
            });
        });
        easyText.on("pointerout", () => {
            this.add.tween({
                targets: easyText,
                scaleX: 1,
                scaleY: 1,
                duration,
            });
        });
        easyText.on("pointerdown", () => {
            this.scene.stop("title");
            this.scene.start("main", { difficulty: "easy" });
        });

        medText.on("pointerover", () => {
            this.add.tween({
                targets: medText,
                scaleX: 1.2,
                scaleY: 1.2,
                duration,
            });
        });
        medText.on("pointerout", () => {
            this.add.tween({
                targets: medText,
                scaleX: 1,
                scaleY: 1,
                duration,
            });
        });
        medText.on("pointerdown", () => {
            this.scene.stop("title");
            this.scene.start("main", { difficulty: "medium" });
        });

        hardText.on("pointerover", () => {
            this.add.tween({
                targets: hardText,
                scaleX: 1.2,
                scaleY: 1.2,
                duration,
            });
        });
        hardText.on("pointerout", () => {
            this.add.tween({
                targets: hardText,
                scaleX: 1,
                scaleY: 1,
                duration,
            });
        });
        hardText.on("pointerdown", () => {
            this.scene.stop("title");
            this.scene.start("main", { difficulty: "hard" });
        });
    }
}
